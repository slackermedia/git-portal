Name:           git-portal
Version:        0.7.2
Release:        2%{?dist}
Summary:        Simple large file management for Git
BuildArch:      noarch
License:        GPLv3
URL:            https://gitlab.com/slackermedia/git-portal
Source0:        https://gitlab.com/slackermedia/%{name}/-/archive/%{version}/%{name}-%{version}.tar.bz2

%description
Git-portal provides large file management for Git with Bash and Git hooks.
It is simple, easy to learn, and supports local file storage and remote storage over SSH (rsync), and rrsync.

%prep
%setup -q
aclocal
autoconf
automake --add-missing

%build
%configure

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%{_bindir}/git-portal
%{_bindir}/git-portal.sh
%{_datadir}/%{name}/hooks/pre-push
%{_datadir}/%{name}/hooks/post-merge
%{_mandir}/man1/%{name}.1.gz

%changelog
* Fri Feb 25 2022 Seth Kenlon <seth@redhat.com> - 0.7.2
- Fixed makefile to not overwrite git-portal script

* Fri Feb 25 2022 Seth Kenlon <seth@redhat.com> - 0.7.1
- Corrected git-portal entry

* Fri Feb 25 2022 Seth Kenlon <seth@redhat.com> - 0.7.0
- Git integration with rev-parse

* Tue Apr 02 2019 Seth Kenlon <skenlon@redhat.com> - 0.6.2
- man pages
- improved protocol detection
- Update 0.6.2

* Mon Apr 01 2019 Seth Kenlon <skenlon@redhat.com> - 0.6.0-1
- RPM build
