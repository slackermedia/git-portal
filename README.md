# git-portal

## What is It?

Simplified blob management for git.

Git-portal is a [portal gun](http://store.steampowered.com/app/400) for Git, written in Bash. 
Use it to whisk away those big binary files (pngs, jpegs, mp4, and so on) to local or remote storage and commit everything else.

## Why?

There are other solutions for this, but they're big and robust and complex.
Git-portal is simple, at the expense of being a little more manual sometimes (it doesn't have automated garbage collection, for instance).
It's ideal for users who need to manage big files that aren't normally agreeable to Git management, but don't want to have to learn a whole new system tacked onto Git.
Git-portal *is* tacked on to Git, but it's really simple to learn because it doesn't do a whole lot.

## Why not?

**Limitations**

* Git-portal *removes* files from being version controlled. If you don't know what that means, *do not use this tool*.
* Git-portal has no automated garbage collection and doesn't actually talk to Git much. If you remove a file from your Git repository, Git-portal happily continues to store the big version of that file. There is no command to find orphans (although you can use ``find`` or other tools to get that information).
* Git-portal relies on Bash-specific functions, so if your workflow has no room for Bash, Git-portal is not for you.
* Git-portal has an option to abuse the contents of `.git/refs/remotes` a little bit, so if you have a custom workflow that also abuses Git remote, there could be a collision, forcing you to choose one or the other.
* There is no `git-mv` equivalent in Git-portal (yet). If you add something to Git-portal and then move its location later, Git-portal is unaware of this move. You can use `git-portal status` to find broken symlinks, but there is no Git-portal command to resolve these kinds of issues for you.

**Benefits**

* Git-portal works locally and remotely
* Git-portal supports SSH and ``rrsync``
* Git-portal is simple and easy to learn, using existing technology instead of reinventing the wheel


## Install

Git-portal requires:

* Git 1.9 or greater
* GNU Bash
* GNU install
* GNU find

You can build from source code.
Building from a Git clone requires these setup steps:

```
$ aclocal
$ autoconf
$ automake --add-missing
```

Building from a tagged release (or completing a build from a Git clone):

```
$ ./configure
$ make
$ make install
```

Or [install the RPM](https://klaatu.fedorapeople.org/git-portal/) on Red Hat (or similar) systems.

On Debian systems, you can convert an RPM to DEB with the ``alien`` command.

## How

`git-portal` creates a `_portal` directory at the top-level of your local Git repository. 
When you add something to `git-portal`, it uses `install` to mirror your hyper-local file system (from Git top-level down) into `_portal`. 
The original file is replaced with a symlink to its authentic self in the `_portal` directory.

When you commit stuff, the symlinks get pushed, but the content remains on your computer.

If you create a Git remote and name it starting with `_portal`, then when you Git push or Git merge (or pull), the contents of your `_portal` directory are synchronized with that remote location.
This is, admittedly, an abuse of the Git remote value, which are intended to store the addresses of Git repositories and not a value for Git-portal to ``rsync`` to.
However, it's convenient and so Git-portal takes advantage of it.

### What about my collaborators?

As long as someone working with you has Git-portal installed, and has access to whatever remote server the contents of `_portal` is stored upon, the contents of all directories stay in sync as expected.
Alternately, you could open up the `_portal` directory to the public so anyone with `rsync` or `wget` or `curl` could fetch the assets.

I've successfully used some iteration of Git-portal on 5 separate multimedia projects with up to 6 collaborators.

## Usage

See docs in the project for details, but basically, this is how you use it:

Build the infrastructure:

```
$ cd ~/my-git-repo
$ git init
$ git portal init

Add a Git remote as usual:

```
$ git remote add origin git@notabug.org:alice/my-git-repo.git
```

Optionally, add a Git-portal remote for automated backups and synchronization of your _portal content:

```
$ git remote add _portal alice@myserver.com:/home/alice/git-assets/_portal
```

Your repository is now configured for Git-portal.

### Daily use of Git-portal

On a daily basis, you will add a big file:

```
$ git portal add bigfile.png
```

Proceed to work with Git as usual.

```
$ git add my-game-code.py bigfile.png
$ git commit -m 'a big PNG file'
$ git push origin HEAD
git-portal syncing... bigfile.png
Counting objects: done.
Delta compression using up to 8 threads.
[...]
1b446a4..96390b6  HEAD -> master
$ 
```

You have actually pushed your ``.py`` code and a symlink representing a big graphic.

If you decide to remove the big file from your project, just use:

```
$ git portal rm bigfile.png
```

This removes both the symlink and the original file.

### How to Apply Git-portal files back to my project

All files in `_portal` are kept in a mirror image of your project directory structure. When you decide to distribute your project and need the real files back in their proper places, you can overlay the contents of ``_portal`` over your build directory:

```
$ cp --remove-destination -rv _portal/* ./BUILD/
```

## Undo

If you started using Git-portal and decide to stop using it, don't panic.
It's easy to get out of Git-portal.

1. Do a `git pull` to make sure everything is properly updated.

2. Move `_portal` out of the repo: `mv _portal /tmp`

3. Copy the content over the symlinks: `cp --remove-destination -rv /tmp/_portal/* /path/to/my-git-repo/`

That's it. Everything that was in `_portal` is now back, as real files instead of symlinks. 
Add them, commit them, and push (or start using `git-lfs`, or whatever you want to do).

## BUGS

* There is no versioning of the bigfiles. If you paint something amazing, add it to `git-portal`, and then accidentally paint over it, there's no reset or checkout to restore that file. When you use Git-portal, you are willfully not versioning your big files. If you are looking for large-file versioning, look elsewhere! **This is important!**
* There's no allowance for `git-mv` (yet).
* Report other bugs to klaatu at mixedsignals.ml

